<?php 

namespace App\Controllers;


use Config\Services;
use App\Models\MusicalModel;
use CodeIgniter\RESTful\ResourceController;

class MusicController extends ResourceController
{
    protected $modelName = 'App\Models\CantanteModel';
    protected $format    = 'json';

    public function index()
    {
        $model = new MusicalModel();
        $genre = $model->findAll();

        return $this->respond($genre);
    }
    

}