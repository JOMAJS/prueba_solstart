-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         8.0.30 - MySQL Community Server - GPL
-- SO del servidor:              Win64
-- HeidiSQL Versión:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para db_cantantes
CREATE DATABASE IF NOT EXISTS `db_cantantes` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `db_cantantes`;

-- Volcando estructura para tabla db_cantantes.musical_genre
CREATE TABLE IF NOT EXISTS `musical_genre` (
  `id` int NOT NULL AUTO_INCREMENT,
  `top` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `descripcion` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Volcando datos para la tabla db_cantantes.musical_genre: ~3 rows (aproximadamente)
DELETE FROM `musical_genre`;
INSERT INTO `musical_genre` (`id`, `top`, `descripcion`) VALUES
	(1, 'POP', 'GENERO DE ROK'),
	(2, 'VALLENATOS', 'GENERO ANTIGUO'),
	(3, 'LATINOS', 'LATINO');

-- Volcando estructura para tabla db_cantantes.singer
CREATE TABLE IF NOT EXISTS `singer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `date_nacimiento` date DEFAULT NULL,
  `biografia` text,
  `foto` varchar(100) DEFAULT NULL,
  `musical_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Volcando datos para la tabla db_cantantes.singer: ~3 rows (aproximadamente)
DELETE FROM `singer`;
INSERT INTO `singer` (`id`, `name`, `date_nacimiento`, `biografia`, `foto`, `musical_id`) VALUES
	(2, 'POP', '1991-07-25', 'DESCROPCION', 'POP.jfif', 1),
	(3, 'LATINOS', '2012-07-25', 'LATINOS DESCRIPCION', 'LATINOS.jpg', 3),
	(4, 'VALLENATOS ', '2023-05-25', 'DESCRIPCION VALLENTOS', 'VALLENATOS.jpg', 2);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
