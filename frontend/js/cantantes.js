$(function() {
    var apiUrl = 'http://pr_solstar.test/Backend/public/restcantante/';
    loadcantants();
    // Manejar el clic en el botón "Abrir formulario"
    $(document).on("click","#openForm",function() {
       var cantantes_id = $(this).data("cantantes_id")
        $.ajax({
            url: 'formulario.html',
            type: 'POST',
            data: {
                cantantes_id: cantantes_id
            },
            success: function(response) {
                $('#contenedor').html(response);
            },
            error: function(xhr, status, error) {
                console.log(error);
            }
        });
    });

   
    // Cargar cantantos desde la API
    function loadcantants() {
        $.ajax({
            url: apiUrl,
            type: 'GET',
            success: function(response) {
                // Limpiar la tabla de cantantos
                $('#cantantTable tbody').empty();

                // Agregar cada cantanto a la tabla
                response.forEach(function(cantant) {
                    var cantantRow = '<tr>';
                   
                    cantantRow += '<td>' + cantant.name + '</td>';
                    cantantRow += '<td>' + cantant.top + '</td>';
                    cantantRow += '<td>' + cantant.date_nacimiento + '</td>';
                    cantantRow += '<td>' + cantant.biografia + '</td>';
                    cantantRow += '<td><img src="http://pr_solstar.test/Backend/public/uploads/' + cantant.foto + '" width="100"></td>';
                    cantantRow += '<td><button class="btn btn-danger deleteBtn" data-id="' + cantant.id + '"> <i class="fa fa-remove"></i> </button></td>';
                    cantantRow += '</tr>';

                   console.log(cantant.name)
                    $('#cantantTable tbody').append(cantantRow);
                });

                // Manejar clic en el botón de eliminar
                $('.deleteBtn').click(function() {
                    var cantantId = $(this).data('id');
                    deletecantant(cantantId);
                });
            },
            error: function(xhr, status, error) {
                console.log(error);
                alert('Hubo un error al cargar los cantantes');
            }
        });
    }

        // Eliminar producto
        function deletecantant(id) {
            if (confirm('¿Estás seguro de eliminar este cantante?')) {
                $.ajax({
                    url: apiUrl + '/' + id,
                    type: 'DELETE',
                    success: function(response) {
                        alert('cantante eliminado correctamente');
                        loadProducts();
                    },
                    error: function(xhr, status, error) {
                        console.log(error);
                        alert('Hubo un error al eliminar el cantante');
                    }
                });
            }
        }

});