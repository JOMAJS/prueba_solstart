<?php

namespace App\Models;

use CodeIgniter\Model;

class MusicalModel extends Model
{

    protected $table         = 'musical_genre';
    protected $primaryKey    = 'id';
    protected $allowedFields = ['top','descripcion'];

    public function getJoinedData()
    {
        return $this->join('musical_genre', 'singer.musical_id = musical_genre.id', 'left')
                    ->findAll();
    }
    
}