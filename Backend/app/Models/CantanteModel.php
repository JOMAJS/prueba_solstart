<?php

namespace App\Models;

use CodeIgniter\Model;

class CantanteModel extends Model
{

    protected $table         = 'singer';
    protected $primaryKey    = 'id';
    protected $allowedFields = ['name','date_nacimiento','biografia','foto','musical_id'];

    public function getJoinedData()
    {
        return $this->join('musical_genre', 'singer.musical_id = musical_genre.id', 'left')
                    ->findAll();
    }
    
}