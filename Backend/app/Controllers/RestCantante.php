<?php 

namespace App\Controllers;


use Config\Services;
use App\Models\CantanteModel;
use CodeIgniter\RESTful\ResourceController;

class RestCantante extends ResourceController
{
    protected $modelName = 'App\Models\CantanteModel';
    protected $format    = 'json';

    public function index()
    {
        $model = new CantanteModel();
        $cantante = $model->getJoinedData();

        return $this->respond($cantante);
    }
    

    public function show($id = null)
    {
        $model = new CantanteModel();
        $cantante = $model->find($id);

        return $this->respond($cantante);
    }

    public function create()
    {
        $model = new CantanteModel();
        $cantante = [
            'name' => $this->request->getPost('name'),
            'date_nacimiento' => $this->request->getPost('date_nacimiento'),
            'biografia' => $this->request->getPost('biografia'),
            'foto'=> $this->request->getPost('foto'),
            'musical_id' => $this->request->getPost('musical_id')
        ];
        // Upload and save image
        $image = $this->request->getFile('foto');
        $image->move(ROOTPATH . 'public/uploads');
        $cantante['foto'] = $image->getName();
        
        $model->insert($cantante);

        return $this->respondCreated($cantante);
    }
  

    public function update($id = null)
    {
        
        $model = new CantanteModel();
        $cantante = [
            'name' => $this->request->getPost('name'),
            'date_nacimiento' => $this->request->getPost('date_nacimiento'),
            'biografia' => $this->request->getPost('biografia'),
            'musical_id' => $this->request->getPost('musical_id')
        ];

        // Upload and save image
        $image = $this->request->getFile('foto');
        $image->move(ROOTPATH . 'public/uploads');
        $cantante['foto'] = $image->getName();

        $model->update($id, $cantante);

        //var_dump( $model->update($id, $cantante))

        return $this->respondCreated($cantante);
    }


    public function delete($id = null)
    {
        $model = new CantanteModel();
        $model->delete($id);

        return $this->respondDeleted();
    }

}